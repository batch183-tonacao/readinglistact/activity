// Reading Activity #1

let firstName = "Juan", lastName = "Dela Cruz";
let age = 17;
let currentAddress = {
	city: "Quezon City",
	province: "Metro Manila"
}

function printUserInfo(firstName,lastName,age,currentAddress){
	return "Hello! I am " + firstName + " " + lastName +", "+ age + " years old, and currently living in " + currentAddress.city +", "+ currentAddress.province+".";
}
console.log(printUserInfo(firstName,lastName,age,currentAddress));

// Reading Activity #2

function ageCheck(age){
	if (age > 17){
		return "You are qualified to vote.";
	}
	else{
		return "Sorry. You're too young to vote.";	
	}
}
console.log(ageCheck(age));

// Reading Activity #3
let userInput = prompt("Enter a Month in number format:");
let monthNum = parseInt(userInput);

function monthCheck(monthNum){
	
	if (monthNum == 1){
		return "Total Number of days for January is " + 31;
		}
	else if (monthNum == 2){
		return "Total Number of days for February is " + 28;
		}
	else if (monthNum == 3){
		return "Total Number of days for March is " + 31;
		}
	else if (monthNum == 4){
		return "Total Number of days for April is " + 30;
		}
	else if (monthNum == 5){
		return "Total Number of days for May is " + 31;
		}
	else if (monthNum == 6){
		return "Total Number of days for June is " + 30;
		}
	else if (monthNum == 7){
		return "Total Number of days for July is " + 30;
		}
	else if (monthNum == 8){
		return "Total Number of days for August is " + 31;
		}
	else if (monthNum == 9){
		return "Total Number of days for September is " + 31;
		}
	else if (monthNum == 10){
		return "Total Number of days for October is " + 30;
		}
	else if (monthNum == 11){
		return "Total Number of days for November is " + 30;
		}
	else if (monthNum == 12){
		return "Total Number of days for December is " + 31;
		}
	else{
	alert("Invalid Input! Please enter the month number between 1 - 12.");
	}
}
console.log(monthCheck(monthNum));

// Reading Activity #4

let yearInput = prompt("Enter year:");
let yearNum = parseInt(yearInput);
let firstCheck = 0, secondCheck =0;

function leapYearCheck(yearNum){
	firstCheck = yearNum % 400;
	secondCheck = yearNum % 4;
	if (firstCheck == 0 || secondCheck == 0){
		return "Year " + yearNum + " is a leap year."
	}
	else{
		return "Year " + yearNum + " is NOT leap year."
	}
}
console.log(leapYearCheck(yearNum));


// Reading Activity #5
let countInput = prompt("Enter a number:");
let countNum = parseInt(countInput);
let counter = 0;

while (counter <= countNum){
	console.log(countNum - counter);
	counter++;
}
	

